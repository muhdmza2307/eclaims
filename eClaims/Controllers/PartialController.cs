﻿using eClaims.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace eClaims.Controllers
{
    public class PartialController : Controller
    {
        // GET: Partial
        public ActionResult GridView()
        {

            GridView objGridView = SessionModel.SessGridView;

            return PartialView(objGridView);
        }
    }
}