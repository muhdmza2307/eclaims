﻿using eClaims.Data;
using eClaims.Models;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace eClaims.Controllers
{
    public class GridViewController : Controller
    {
        private static GridDLL _gridData = new GridDLL();
        public void GridViewListingReturn(string gridId)
        {
            try
            {
                if (gridId == "CL01")
                    GetClaimList(Convert.ToString(SessionModel.SessGridView.rowPerPage), Convert.ToInt32(SessionModel.SessGridView.CurPage), gridId);
                else
                    GetClaimsPreviewList(Convert.ToString(SessionModel.SessGridView.rowPerPage), Convert.ToInt32(SessionModel.SessGridView.CurPage), gridId);
            }
            catch (Exception ex)
            {
                throw ex;
            }

        }

        public ActionResult GetClaimList(string rowPerPage, int CurPage, string gridId)
        {
            GridView objGridView = SessionModel.SessGridView;

            DataTable dtGridStp = _gridData.GetGridViewSetup(gridId);

            if (dtGridStp != null && dtGridStp.Rows.Count > 0)
            {
                objGridView.GridViewID = dtGridStp.Rows[0]["vaGridId"].ToString();
                objGridView.GridHeaderList = dtGridStp.Rows[0]["vaHeadList"].ToString();
                objGridView.GridBodyList = dtGridStp.Rows[0]["vaBodyList"].ToString();
                string gridSetting = dtGridStp.Rows[0]["vaGridSetting"].ToString();
                string[] arrGridSett= gridSetting.Split('|');
                objGridView.hasCheckbox = Convert.ToBoolean(Convert.ToInt32(arrGridSett[0].ToString())); 
                objGridView.hasActionBtn = Convert.ToBoolean(Convert.ToInt32(arrGridSett[1].ToString()));
                objGridView.hasTotalFinal = Convert.ToBoolean(Convert.ToInt32(arrGridSett[2].ToString()));
                objGridView.GridViewStoreProc = dtGridStp.Rows[0]["vaGridSpName"].ToString();
                objGridView.ColumnHide = dtGridStp.Rows[0]["vaClmnHide"].ToString();

                objGridView.rowPerPage = rowPerPage;
                objGridView.CurPage = CurPage;
                objGridView.dtGridView = _gridData.GetClaimList(objGridView.GridViewStoreProc);

                string ActionLink = "Claim~ViewClaim~claimId=$param";
                string[] arrActionLink = ActionLink.Split('|');

                objGridView.LinkList = new List<Link>();

                string[] arrObjLink = new string[]{ };

                for (int i=0; i < arrActionLink.Count(); i++)
                {
                    arrObjLink = arrActionLink[i].ToString().Split('~');


                    objGridView.LinkList.Add(new Link { CtrlName = arrObjLink[0].ToString(), ActionName = arrObjLink[1].ToString(), Param = arrObjLink[2].ToString(), ParamVal = "" });

                }

                
                if (arrObjLink.Count() > 2)
                {
                    
                    foreach (var item in objGridView.LinkList)
                    {
                        for (int k = 0; k < objGridView.dtGridView.Rows.Count; k++)
                        {
                            if (k == 0)
                            {
                                item.ParamVal = item.ParamVal + objGridView.dtGridView.Rows[k]["iClaimId"].ToString();
                            }
                            else
                            {
                                item.ParamVal = item.ParamVal + "," +objGridView.dtGridView.Rows[k]["iClaimId"].ToString();
                            }
                        }
                           
                        
                    }
                   
                }

            }

            return PartialView(@"~/Views/Partial/GridView.cshtml", objGridView);
        }

        public ActionResult GetClaimsPreviewList(string rowPerPage, int CurPage, string gridId)
        {
            GridView objGridView = SessionModel.SessGridView;

            DataTable dtGridStp = _gridData.GetGridViewSetup(gridId);

            if (dtGridStp != null && dtGridStp.Rows.Count > 0)
            {
                objGridView.GridViewID = dtGridStp.Rows[0]["vaGridId"].ToString();
                objGridView.GridHeaderList = dtGridStp.Rows[0]["vaHeadList"].ToString();
                objGridView.GridBodyList = dtGridStp.Rows[0]["vaBodyList"].ToString();
                string gridSetting = dtGridStp.Rows[0]["vaGridSetting"].ToString();
                string[] arrGridSett = gridSetting.Split('|');
                objGridView.hasCheckbox = Convert.ToBoolean(Convert.ToInt32(arrGridSett[0].ToString()));
                objGridView.hasActionBtn = Convert.ToBoolean(Convert.ToInt32(arrGridSett[1].ToString()));
                objGridView.hasTotalFinal = Convert.ToBoolean(Convert.ToInt32(arrGridSett[2].ToString()));
                objGridView.GridViewStoreProc = dtGridStp.Rows[0]["vaGridSpName"].ToString();
                objGridView.ColumnHide = dtGridStp.Rows[0]["vaClmnHide"].ToString();
            }

            objGridView.rowPerPage = rowPerPage;
            objGridView.CurPage = CurPage;
            objGridView.dtGridView = _gridData.GetClaimsSubmissionPreview(objGridView.GridViewStoreProc,objGridView.StoreProcParam);


            return PartialView(@"~/Views/Partial/GridView.cshtml", objGridView);
        }

    }
}