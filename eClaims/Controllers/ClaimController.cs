﻿using eClaims.Data;
using eClaims.Models;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace eClaims.Controllers
{
    public class ClaimController : Controller
    {
        private static ClaimDLL _claimData = new ClaimDLL();
        public ActionResult AddClaim()
        {
            ClaimInfo objClaimInfo = new ClaimInfo();
            objClaimInfo.pageTitle = "Add Claim";
            GetScreenSetup(objClaimInfo);

            return View("ClaimInfo", objClaimInfo);
        }

        public void GetScreenSetup(ClaimInfo objClaimInfo)
        {
            DataTable dtCostCenter = _claimData.GetListofCostCenter();

            DataTable dtGlCode = _claimData.GetListofGlCode();

            DataTable dtCurrency = _claimData.GetListofCurrency();

            objClaimInfo.listCostCenter = new List<SelectListItem>();
            objClaimInfo.listCostCenter = (from DataRow dr in dtCostCenter.Rows
                                           select new SelectListItem()
                                           {
                                               Value = dr["iCostId"].ToString(),
                                               Text = dr["vaDesc"].ToString(),
                                           }).ToList();

            objClaimInfo.listGlCode = new List<SelectListItem>();
            objClaimInfo.listGlCode = (from DataRow dr in dtGlCode.Rows
                                       select new SelectListItem()
                                       {
                                           Value = dr["iGlId"].ToString(),
                                           Text = dr["vaDesc"].ToString(),
                                       }).ToList();

            objClaimInfo.listCurrency = new List<SelectListItem>();
            objClaimInfo.listCurrency = (from DataRow dr in dtCurrency.Rows
                                         select new SelectListItem()
                                         {
                                             Value = dr["iCurr"].ToString(),
                                             Text = dr["vaCurr"].ToString(),
                                         }).ToList();
        }

       [HttpPost]
        public ActionResult AddClaim(ClaimInfo objClaimInfo)
        {
            if (ModelState.IsValid)
            {
                string rtnCode = _claimData.AddNewClaim(objClaimInfo);

                if (rtnCode == "0")
                    return RedirectToAction("SubmittedClaim", "Claim");
            }

            GetScreenSetup(objClaimInfo);

            return View("ClaimInfo", objClaimInfo);
        }

        public ActionResult ViewClaim(int claimId)
        {
            ClaimInfo objClaimInfo = new ClaimInfo();

            DataTable dtClaimInfo = _claimData.GetClaimInfo(claimId);

            if (dtClaimInfo != null && dtClaimInfo.Rows.Count > 0 && Convert.ToInt32(dtClaimInfo.Rows[0]["iClaimId"].ToString()) == claimId)
            {
                objClaimInfo.pageMode = 2;//view
                objClaimInfo.pageTitle = "View Claim";
                objClaimInfo.iClaimId = claimId;
                objClaimInfo.decExRate = decimal.Parse(dtClaimInfo.Rows[0]["decExRate"].ToString());
                objClaimInfo.decGST = decimal.Parse(dtClaimInfo.Rows[0]["decGST"].ToString());
                objClaimInfo.dtTrnDate = Convert.ToDateTime(dtClaimInfo.Rows[0]["dtTrnDate"].ToString());
                objClaimInfo.mnAmount = decimal.Parse(dtClaimInfo.Rows[0]["mnAmt"].ToString());
                objClaimInfo.mnTotalAmt = decimal.Parse(dtClaimInfo.Rows[0]["mnTotalAmt"].ToString());
                objClaimInfo.vaCostCtr = dtClaimInfo.Rows[0]["vaCostCtr"].ToString();
                objClaimInfo.vaCurr = dtClaimInfo.Rows[0]["vaCurr"].ToString();
                objClaimInfo.vaDesc = dtClaimInfo.Rows[0]["vaDesc"].ToString();
                objClaimInfo.vaGLCode = dtClaimInfo.Rows[0]["vaGLCode"].ToString();
                objClaimInfo.mnGSTAmt = decimal.Parse(dtClaimInfo.Rows[0]["mnGSTAmt"].ToString());
            }

            return View("ClaimInfo", objClaimInfo);
        }

        public ActionResult SubmittedClaim()
        {
            GridView objGridView = new GridView();
            objGridView.SortDefaultValue = "ClaimId";
            objGridView.SortValue = objGridView.SortDefaultValue + " DESC";

            SessionModel.SessGridView = objGridView;
            GridViewController objGridViewController = new GridViewController();
            objGridViewController.GridViewListingReturn("CL01");

            return View();
        }

        [HttpPost]
        public ActionResult PreviewSubmissionClaim(string slctClaim)
        {
            GlobalModel objGlobalInfo = new GlobalModel();

            UserInfo objUsrInfo = new UserInfo();
            BankInfo objBankInfo = new BankInfo();
            PageInfo objPageInfo = new PageInfo();

            objPageInfo.pageTitle = "Expense Claims";

            DataTable dtUserBankInfo = _claimData.GetUserBankInfo(2);

            if (dtUserBankInfo != null && dtUserBankInfo.Rows.Count > 0)
            {
                objUsrInfo.vaFullName = dtUserBankInfo.Rows[0]["vaFullName"].ToString();
                objUsrInfo.vaBranchCode = dtUserBankInfo.Rows[0]["vaBranchCode"].ToString();

                objBankInfo.vaBankCode = dtUserBankInfo.Rows[0]["vaBankCode"].ToString();
                objBankInfo.vaBankAccName = dtUserBankInfo.Rows[0]["vaBankAccName"].ToString();
                objBankInfo.vaBankAccNo = dtUserBankInfo.Rows[0]["vaBankAccNo"].ToString();
            }

            objGlobalInfo.clsPageInfo = objPageInfo;
            objGlobalInfo.clsUserInfo = objUsrInfo;
            objGlobalInfo.clsBankInfo = objBankInfo;

            GridView objGridView = new GridView();
            objGridView.SortDefaultValue = "ClaimId";
            objGridView.StoreProcParam = slctClaim;

            SessionModel.SessGridView = objGridView;
            GridViewController objGridViewController = new GridViewController();
            objGridViewController.GridViewListingReturn("CL02");


            return View(objGlobalInfo);
        }
    }
}