﻿using eClaims.Helper;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;

namespace eClaims.Data
{
    public class GridDLL
    {
        private static SqlHelper _eClaimsSqlHelper = new SqlHelper(ConfigurationManager.ConnectionStrings["eClaims"].ConnectionString);
        protected internal DataTable GetClaimList(string spName)
        {
            SqlParameter[] SQLParam = new SqlParameter[] {
                new SqlParameter("@ip_iUsrId", 2),
                new SqlParameter("@op_ReturnCode", SqlDbType.Int, 10),
                new SqlParameter("@op_ErrorMsg", SqlDbType.VarChar, 1024)
            };

            SQLParam[1].Direction = ParameterDirection.Output;
            SQLParam[2].Direction = ParameterDirection.Output;

            DataTable dtGridViewList = _eClaimsSqlHelper.SqlExecute_DataTable(spName, SQLParam);

            string strReturnCode = Convert.ToString(SQLParam[1].Value);
            string strErrorMsg = Convert.ToString(SQLParam[2].Value);
            if (strReturnCode != "0")
            {
                throw new Exception(strErrorMsg);
            }

            return dtGridViewList;
        }

        protected internal DataTable GetClaimsSubmissionPreview(string spName, string claims)
        {
            SqlParameter[] SQLParam = new SqlParameter[] {
                new SqlParameter("@ip_iClaims", claims),
                new SqlParameter("@op_ReturnCode", SqlDbType.Int, 10),
                new SqlParameter("@op_ErrorMsg", SqlDbType.VarChar, 1024)
            };

            SQLParam[1].Direction = ParameterDirection.Output;
            SQLParam[2].Direction = ParameterDirection.Output;

            DataTable dtGridViewList = _eClaimsSqlHelper.SqlExecute_DataTable(spName, SQLParam);

            string strReturnCode = Convert.ToString(SQLParam[1].Value);
            string strErrorMsg = Convert.ToString(SQLParam[2].Value);
            if (strReturnCode != "0")
            {
                throw new Exception(strErrorMsg);
            }

            return dtGridViewList;
        }

        protected internal DataTable GetGridViewSetup(string gridId)
        {
            SqlParameter[] SQLParam = new SqlParameter[] {
                new SqlParameter("@ip_vaGridId", gridId),
                new SqlParameter("@op_ReturnCode", SqlDbType.Int, 10),
                new SqlParameter("@op_ErrorMsg", SqlDbType.VarChar, 1024)
            };

            SQLParam[1].Direction = ParameterDirection.Output;
            SQLParam[2].Direction = ParameterDirection.Output;

            DataTable dtGridStp = _eClaimsSqlHelper.SqlExecute_DataTable("STP_Get_GridSetupById", SQLParam);

            string strReturnCode = Convert.ToString(SQLParam[1].Value);
            string strErrorMsg = Convert.ToString(SQLParam[2].Value);
            if (strReturnCode != "0")
            {
                throw new Exception(strErrorMsg);
            }

            return dtGridStp;
        }

        
    }
}