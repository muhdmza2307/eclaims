﻿using eClaims.Helper;
using eClaims.Models;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;

namespace eClaims.Data
{
    public class ClaimDLL
    {
        private static SqlHelper _eClaimsSqlHelper = new SqlHelper(ConfigurationManager.ConnectionStrings["eClaims"].ConnectionString);

        protected internal DataTable GetClaimInfo(int claimId)
        {
            SqlParameter[] SQLParam = new SqlParameter[] {
                new SqlParameter("@ip_iClaimId", claimId),
                new SqlParameter("@op_ReturnCode", SqlDbType.Int, 10),
                new SqlParameter("@op_ErrorMsg", SqlDbType.VarChar, 1024)
            };

            SQLParam[1].Direction = ParameterDirection.Output;
            SQLParam[2].Direction = ParameterDirection.Output;

            DataTable dtClaimInfo = _eClaimsSqlHelper.SqlExecute_DataTable("TRN_Get_ClaimDetailsByClaimId", SQLParam);

            string strReturnCode = Convert.ToString(SQLParam[1].Value);
            string strErrorMsg = Convert.ToString(SQLParam[2].Value);
            if (strReturnCode != "0")
            {
                throw new Exception(strErrorMsg);
            }

            return dtClaimInfo;
        }

        protected internal DataTable GetListofCostCenter()
        {
            SqlParameter[] SQLParam = new SqlParameter[] { };

            DataTable dtCostCenter = _eClaimsSqlHelper.SqlExecute_DataTable("MST_Get_CostCenterList", SQLParam);
            return dtCostCenter;
        }

        protected internal DataTable GetListofGlCode()
        {
            SqlParameter[] SQLParam = new SqlParameter[] { };

            DataTable dtGlCode = _eClaimsSqlHelper.SqlExecute_DataTable("MST_Get_GLCcodeList", SQLParam);
            return dtGlCode;
        }

        protected internal DataTable GetListofCurrency()
        {
            SqlParameter[] SQLParam = new SqlParameter[] { };

            DataTable dtCurrency = _eClaimsSqlHelper.SqlExecute_DataTable("MST_Get_CurrencyList", SQLParam);
            return dtCurrency;
        }

        protected internal DataTable GetUserBankInfo(int userId)
        {
            SqlParameter[] SQLParam = new SqlParameter[] {
                new SqlParameter("@ip_iUserId", userId),
                new SqlParameter("@op_ReturnCode", SqlDbType.Int, 10),
                new SqlParameter("@op_ErrorMsg", SqlDbType.VarChar, 1024)
            };

            SQLParam[1].Direction = ParameterDirection.Output;
            SQLParam[2].Direction = ParameterDirection.Output;

            DataTable dtUserBankInfo = _eClaimsSqlHelper.SqlExecute_DataTable("TRN_Get_UserBankDetails", SQLParam);

            string strReturnCode = Convert.ToString(SQLParam[1].Value);
            string strErrorMsg = Convert.ToString(SQLParam[2].Value);
            if (strReturnCode != "0")
            {
                throw new Exception(strErrorMsg);
            }

            return dtUserBankInfo;
        }

        protected internal string AddNewClaim(ClaimInfo objClaimInfo)
        {
            SqlParameter[] SQLParam = new SqlParameter[] {
                new SqlParameter("@ip_dtTrnDate", objClaimInfo.dtTrnDate),
                new SqlParameter("@ip_iCostCtr", objClaimInfo.iCostCtr),
                new SqlParameter("@ip_vaDesc", objClaimInfo.vaDesc),
                new SqlParameter("@ip_iCurr", objClaimInfo.iCurr),
                new SqlParameter("@ip_mnAmnt", objClaimInfo.mnAmount),
                new SqlParameter("@ip_decGST", objClaimInfo.decGST),
                new SqlParameter("@ip_decExRate", objClaimInfo.decExRate),
                new SqlParameter("@mnTotalAmt", objClaimInfo.mnTotalAmt),
                new SqlParameter("@ip_iGlId", objClaimInfo.iGlId),
                new SqlParameter("@ip_mnGSTAmt", objClaimInfo.mnGSTAmt),
                new SqlParameter("@ip_iUserId", 2),
                new SqlParameter("@op_ReturnCode", SqlDbType.Int, 10),
                new SqlParameter("@op_ErrorMsg", SqlDbType.VarChar, 1024)
            };

            SQLParam[11].Direction = ParameterDirection.Output;
            SQLParam[12].Direction = ParameterDirection.Output;

            _eClaimsSqlHelper.SqlExecuteNonQuery("sp_ADD_TRN_Claim", SQLParam);

            string strReturnCode = Convert.ToString(SQLParam[11].Value);
            string strMsg = Convert.ToString(SQLParam[12].Value);
            if (strReturnCode != "0")
            {
                throw new Exception(strMsg);
            }
            return strReturnCode;
        }
    }
} 