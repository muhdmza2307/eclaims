﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;

namespace eClaims.Models
{
    public class GridView
    {
        public DataTable dtGridView { get; set; }
        public string GridViewID { get; set; } = "";
        public string GridHeaderList { get; set; } = "";
        public string GridBodyList { get; set; } = "";
        public string rowPerPage { get; set; } = "10";
        public int CurPage { get; set; } = 1;
        public int Pagesize { get; set; } = 10;
        public int RecordNo { get; set; } = 100;
        public int[] ColumnSort { get; set; }
        public int[] ColumnCheckBox { get; set; }
        public int[] ColumnHyperLink { get; set; }
        public string GridViewStoreProc { get; set; } = "";
        public string SortDefaultValue { get; set; } = "";
        public string SortValue { get; set; } = "";
        public bool hasCheckbox { get; set; } = false;
        public bool hasActionBtn { get; set; } = false;
        public bool hasTotalFinal { get; set; } = false;
        public string ColumnHide { get; set; }
        public string ActionLink { get; set; }
        public string[] ArrActionLink { get; set; }
        public string[] ArrParamsLink { get; set; }
        public List<string> ArrParamsLinkVal { get; set; }
        public List<Link> LinkList { get; set; }
        public string StoreProcParam { get; set; }
    }

    public class Link
    {
        public string CtrlName { get; set; }
        public string ActionName { get; set; }
        public string Param { get; set; }
        public string ParamVal { get; set; }
    }
}