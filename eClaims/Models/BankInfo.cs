﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace eClaims.Models
{
    public class BankInfo
    {
        [Display(Name = "Bank Code")]
        public string vaBankCode { get; set; }

        [Display(Name = "Bank Account No")]
        public string vaBankAccNo { get; set; }

        [Display(Name = "Bank Account Name")]
        public string vaBankAccName { get; set; }
    }
}