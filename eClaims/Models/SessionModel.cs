﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace eClaims.Models
{
    public class SessionModel
    {
        public static GridView SessGridView
        {
            get
            {
                GridView objGrid = new GridView();
                if (HttpContext.Current.Session["SessGridView"] != null)
                {
                    objGrid = (GridView)HttpContext.Current.Session["SessGridView"];
                }
                else
                {
                    HttpContext.Current.Session["SessGridView"] = objGrid;
                }
                return objGrid;

            }
            set
            {
                HttpContext.Current.Session["SessGridView"] = value;
            }
        }
    }
}