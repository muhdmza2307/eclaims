﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace eClaims.Models
{
    public class ClaimInfo : PageInfo
    {
        public int iClaimId { get; set; } = 1;

        [Required(ErrorMessage = "Date Of Transaction is required.")]
        [Display(Name = "Date Of Transaction")]
        public DateTime dtTrnDate { get; set; }

        [Required(ErrorMessage = "Cost Center is required.")]
        public int iCostCtr { get; set; }

        [Display(Name = "Cost Center")]
        public string vaCostCtr { get; set; }

        [Required(ErrorMessage = "GL Code is required.")]
        public int iGlId { get; set; }

        [Display(Name = "GL Code")]
        public string vaGLCode { get; set; }

        [Required(ErrorMessage = "Description is required.")]
        [Display(Name = "Description")]
        public string vaDesc { get; set; }

        [Required(ErrorMessage = "Currency is required.")]
        public int iCurr { get; set; }

        [Display(Name = "Currency")]
        public string vaCurr { get; set; }

        [Required(ErrorMessage = "Amount is required.")]
        [Display(Name = "Amount")]
        public decimal? mnAmount { get; set; }

        [Display(Name = "GST")]
        public decimal? decGST { get; set; }

        [Display(Name = "GST Amount")]
        public decimal? mnGSTAmt { get; set; }

        [Display(Name = "Exchange Rate")]
        public decimal? decExRate { get; set; }

        [Required(ErrorMessage = "Total Amount is required.")]
        [Display(Name = "Total Amount")]
        public decimal? mnTotalAmt { get; set; }

        public List<SelectListItem> listCostCenter { get; set; }
        public List<SelectListItem> listGlCode { get; set; }
        public List<SelectListItem> listCurrency { get; set; }
    }

    //public class CostCentre
    //{
    //    public int iCostId { get; set; }
    //    public string vaCostDesc { get; set; }
    //}

    //public class GlCode
    //{
    //    public int iGlId { get; set; }
    //    public string vaGlDesc { get; set; }
    //}
}