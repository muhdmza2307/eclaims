﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace eClaims.Models
{
    public class PageInfo
    {
        public int pageMode { get; set; } = 1;
        public string pageTitle { get; set; } = "";
    }
}