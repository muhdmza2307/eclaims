﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace eClaims.Models
{
    public class UserInfo
    {
        [Display(Name = "User Name")]
        public string vaUsrName { get; set; }

        [Display(Name = "Full Name")]
        public string vaFullName { get; set; }

        [Display(Name = "Branch Code")]
        public string vaBranchCode { get; set; }
    }
}