﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace eClaims.Models
{
    public class GlobalModel
    {
        public ClaimInfo ClsClaimInfo { get; set; }

        public UserInfo clsUserInfo { get; set; }

        public BankInfo clsBankInfo { get; set; }

        public GridView clsGridInfo { get; set; }

        public PageInfo clsPageInfo { get; set; }
    }
}